/***
以下是GM脚本转chrome扩展的中间层
***/
onReadyGM = function() {};

function isChromeExtension() {
	return (typeof chrome == 'object') && (typeof chrome.extension == 'object')
};

function chromeCompatible() {
	var localStorage, isInitialized = false,
		port = chrome.extension.connect();
	Connection = (function() {
		var callbackList = {},
			callbackId = 0;

		function callbackResponse(id, response) {
			callbackList[id](response);
			delete callbackList[id]
		};

		function registCallBack(callback) {
			callbackList[++callbackId] = callback;
			return callbackId
		};
		return {
			callbackResponse: callbackResponse,
			registCallBack: registCallBack
		}
	})();

	function onInitializedGM(response) {
		localStorage = response;
		isInitialized = true;
		(onReadyGM ||
		function() {})()
	};
	GM_xmlhttpRequest = function(opt) {
		port.postMessage({
			action: "xhr",
			args: [opt, Connection.registCallBack(opt.onload)]
		})
	};
	GM_log = function(message) {
		console.log(message)
	};
	GM_setValue = function(key, value) {
		localStorage[key] = value;
		port.postMessage({
			action: "setValue",
			args: [key, value]
		})
	};
	GM_getValue = function(key, def) {
		var val = localStorage[key];
		val = (val == 'true') ? true : (val == 'false' ? false : val);
		if (val == undefined && def != undefined) {
			return def
		};
		return val
	};
	GM_registerMenuCommand = function(menuText, callbackFunction) {};
	GM_addStyle = function(css) {
		var parent = document.getElementsByTagName('head')[0] || document.documentElement;
		var style = document.createElement('style');
		style.type = 'text/css';
		var textNode = document.createTextNode(css);
		style.appendChild(textNode);
		parent.appendChild(style)
	};
	if (typeof(unsafeWindow) == 'undefined') {
		unsafeWindow = window
	};
	GM_openInTab = function(url) {
		unsafeWindow.open(url, '')
	};
	port.onMessage.addListener(function(res) {
		(Connection[res.action] ||
		function() {}).apply(Connection, res.args)
	});
	port.postMessage({
		action: "initGM",
		args: Connection.registCallBack(onInitializedGM)
	})
};
chromeCompatible();